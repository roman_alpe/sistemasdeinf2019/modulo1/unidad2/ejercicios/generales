﻿USE concursos;
/** Indica los concursantes que pesan más de 90kg y miden menos de 180 cms**/


SELECT nombre 
  FROM concursantes
  WHERE peso>90 AND altura<180;

/** Indica la provincia que tenga más de dos poblaciones**/


SELECT provincia 
  FROM concursantes
  GROUP BY provincia
  HAVING COUNT(poblacion)>2;

/** Indicar el año en el que han nacido más de un concursante que sea de Cantabria**/
  SELECT 
   fechaNacimiento, c.nombre 
    FROM concursantes c
    WHERE c.provincia='Cantabria'
    GROUP BY c.fechaNacimiento
    HAVING COUNT(*);
/** Calcular la altura media por provincia y el peso máximo de los concursantes de esa provincia**/
    
  SELECT AVG(altura),MAX(c.peso), c.provincia 
    FROM concursantes c
    GROUP BY c.provincia;
  /** Indica el mes que tenga más de 1 concursante que pese más de 70kg e indicar el peso medio de los concursantes**/
  SELECT AVG(peso),MONTH(c.fechaNacimiento) FROM concursantes c
    WHERE peso>70
    GROUP BY MONTH(c.fechaNacimiento)
    HAVING COUNT(*)>1;