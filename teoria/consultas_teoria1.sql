﻿USE teoria1;

/*
  1- Nº de coches que ha alquilado el usuario 1
*/

SELECT COUNT(*) nalquileres 
  FROM alquileres a WHERE a.usuario=1;

SELECT COUNT(DISTINCT a.coche) ncoches 
  FROM alquileres a WHERE a.usuario=1;

/*
  2- Nº de alquileres por mes
*/

SELECT MONTH(a.fecha), COUNT(*) numero 
  FROM alquileres a
  GROUP BY MONTH(a.fecha);
/*
  3- el nº de usuarios por sexo
*/

SELECT u.sexo, COUNT(*) nusuarios 
  FROM usuarios u
  GROUP BY u.sexo;
/*
  4-Nº de alquileres de coches por color 
*/
SELECT c.color, COUNT(*) numero 
  FROM coches c 
  JOIN alquileres a 
  ON c.codigoCoche = a.coche
  GROUP BY c.color;
/*
  5-Nº de marcas 
*/  
SELECT COUNT(DISTINCT c.marca) numero
FROM coches c;

/*
  6-Nº de marcas de coches alquilados
*/
-- Opción1

SELECT c.marca, COUNT(*) numero 
  FROM coches c 
  JOIN alquileres a 
  ON c.codigoCoche = a.coche
  GROUP BY c.marca;

-- Opción2

SELECT COUNT(DISTINCT c.marca) numero 
  FROM coches c
  JOIN alquileres a 
  ON c.codigoCoche = a.coche;
  
  








-- SELECT c.marca, COUNT(*) numero
-- FROM coches c
-- GROUP BY c.marca;