﻿USE teoria1;

/* Consultas de repaso de combinaciones

-- 1. Indicar el nombre de las marcas que se hayan alquilado coches

  -- Sin optimizar*/
  
  SELECT DISTINCT c.marca 
    FROM alquileres a 
    JOIN coches c 
    ON a.coche = c.codigoCoche;

  /* Optimizada
   C1 - Coches alquilados*/
  
  SELECT DISTINCT a.coche 
    FROM alquileres a;

  -- Consulta final -  Marcas de los coches alquilados
  
  SELECT DISTINCT c.marca 
    FROM (
      SELECT DISTINCT a.coche 
        FROM alquileres a
    ) C1
    JOIN coches c 
    ON C1.coche = c.codigoCoche;

  /* 2. Indicar los nombres de los usuarios que hayan alquilado algun coche
  
   C1 - Usuarios han alquilado*/
  
  SELECT DISTINCT a.usuario 
    FROM alquileres a;

  -- Consulta final - nombre de los usuarios que han alquilado coches
  
  SELECT DISTINCT u.nombre 
    FROM (
      SELECT DISTINCT a.usuario 
        FROM alquileres a
    ) C1 
    JOIN usuarios u 
    ON C1.usuario = u.codigoUsuario;

-- 3. Coches que no han sido alquilados

  SELECT c.codigoCoche 
    FROM coches c 
    LEFT JOIN alquileres a 
    ON c.codigoCoche = a.coche 
    WHERE a.coche IS NULL;

-- Optimizada

  -- c1
  SELECT DISTINCT a.coche FROM alquileres a;
  -- Final
    
    SELECT c.codigoCoche  
      FROM coches c LEFT JOIN 
      (
      SELECT DISTINCT a.coche FROM alquileres a
      ) c1 
      ON c.codigoCoche = c1.coche 
      WHERE c1.coche IS NULL;
