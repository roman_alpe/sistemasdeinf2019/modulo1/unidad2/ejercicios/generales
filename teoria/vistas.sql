﻿USE teoria1;

/*
  2- Nombres de los usuarios que han alquilado coches
*/

-- sin optimizar
  
  CREATE OR REPLACE VIEW consulta2 AS
  SELECT DISTINCT u.nombre FROM usuarios u JOIN alquileres a
    ON u.codigoUsuario=a.usuario

    SELECT * FROM consulta2;

  -- Optimizada

    -- c1 : los usuarios que han alquilado coches
  CREATE OR REPLACE VIEW c1Consulta2 AS
    SELECT DISTINCT a.usuario FROM alquileres a;
  
  -- Final
  
  CREATE OR REPLACE VIEW consulta2 AS
    SELECT DISTINCT u.nombre FROM 
    c1Consulta2 c1
      JOIN usuarios u
      ON c1.usuario= u.codigoUsuario;
  
  
  -- Ver resultado de la consulta
  
  SELECT * FROM consulta2 c;
     
  /*
   3- Coches que no han sido alquilados
 */

    
-- Optimizada

  CREATE OR REPLACE VIEW c1Consulta3 AS
    SELECT c.codigoCoche FROM coches c;
  
  CREATE OR REPLACE VIEW consulta3 AS
    SELECT c.codigoCoche 
      FROM coches c 
      LEFT JOIN alquileres a 
      ON c.codigoCoche = a.coche 
      WHERE a.coche IS NULL;


-- CREATE OR REPLACE VIEW c1Consulta3 AS
--  SELECT c.codigoCoche FROM coches c;



-- Optimizada

  CREATE OR REPLACE VIEW c1Consulta3 AS 
    SELECT DISTINCT a.coche FROM alquileres a;

  CREATE OR REPLACE VIEW consulta3 AS
  SELECT c.codigoCoche  
        FROM coches c LEFT JOIN 
        c1Consulta3 c1 
        ON c.codigoCoche = c1.coche 
        WHERE c1.coche IS NULL;


-- SELECT DISTINCT a.coche FROM alquileres a