﻿/**
  Cuántas etapas ha ganado cada ciclista
  dorsal, numeroEtapas
  1,3
  2,1**/
  
  
  /**SELECT e.numetapa, e.dorsal FROM etapa e
    GROUP BY e.numetapa,e.dorsal
    HAVING COUNT(*);**/
  
  SELECT e.dorsal, COUNT(*) numeroEtapas
    FROM etapa e
    GROUP BY e.dorsal;

  
  
  
  
  
  
  /**
  nombre de los ciclistas que han ganado mas de 2 etapas.
  
  
  **/
    
SELECT e.dorsal, COUNT(*) numeroEtapas
    FROM etapa e
    GROUP BY e.dorsal
    HAVING numeroEtapas>2;

    SELECT e.dorsal
    FROM etapa e
    GROUP BY e.dorsal
    HAVING COUNT(*)>2;

    SELECT dorsal FROM (
    SELECT e.dorsal, COUNT(*) numeroEtapas
    FROM etapa e
    GROUP BY e.dorsal
    HAVING numeroEtapas>2
) c1;

/**
  nombre de los ciclistas que han ganado mas de 2 etapas.
  
  
  **/

    SELECT c.nombre FROM ( 
      SELECT e.dorsal
        FROM etapa e
        GROUP BY e.dorsal
        HAVING COUNT(*)>2
    ) c1 JOIN ciclista c USING(dorsal);

    /* El nombre de ciclista que tiene más edad */
      
      SELECT (*) 
        FROM ciclista c
        ORDER BY edad DESC LIMIT 1;
      
     -- c1 : edad máxima
     
     SELECT MAX(edad) maxima
      FROM ciclista c;
    
    -- solución 1: join
     
     SELECT c.nombre FROM ciclista c
      JOIN (SELECT MAX(edad) maxima
      FROM ciclista c)  c1
      ON maxima=c.edad;
        