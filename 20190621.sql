﻿USE ciclistas;

/**
  proyeccion
 **/
  
  SELECT DISTINCT c.nombre FROM ciclista AS c;

  SELECT DISTINCT c.nombre FROM ciclista c;
  
  SELECT DISTINCT ciclista.nombre FROM ciclista;

  SELECT DISTINCT nombre FROM ciclista;

  /**
  proyeccion
 **/
    
    SELECT
        *
    FROM
        ciclista c
    WHERE
        c.edad<30;
      
    
    SELECT * FROM ciclista c WHERE c.edad<30;
    
/**
  combino ambos operadores
 **/


  SELECT DISTINCT
      c.nombre
    FROM
      ciclista c
    where
      c.edad<30;

 -- indícame las edades de los ciclistas de Banesto
  
  SELECT DISTINCT
      c.edad
    FROM
      ciclista c
    WHERE
      c.nomequipo='Banesto';

  SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo='Banesto';

  
  SELECT 
    DISTINCT c.nomequipo 
  FROM 
    ciclista c 
  
   WHERE 
    c.edad<30;

  
/**
    and y or
**/

-- listar los nombres de los ciclistas
-- cuya edad esta entre 30 y 32 (inclusive)

    
   SELECT
    
   DISTINCT c.nombre

   FROM
    ciclista c
   WHERE
    c.edad>=30 AND c.edad<=32;

   
   
   -- modificar con los operadores extendidos


  SELECT
    
   DISTINCT c.nombre

   FROM
    ciclista c
   WHERE
    c.edad BETWEEN 30 AND 32;

  -- 
     
   -- listar los equipos que tengan ciclistas 
   -- menores de 31 años y que empiece su nombre por M

  SELECT 

    DISTINCT c.nomequipo
  
  FROM 
    ciclista c
  
  WHERE 
    c.edad<31 AND c.nombre LIKE 'M%'; 

  SELECT 

    DISTINCT c.nomequipo
  
  FROM 
    ciclista c
  
  WHERE 
    c.edad<31 OR c.nombre LIKE 'M%';
    
-- listarme los ciclistas de Banesto y de Kelme

 SELECT *

  FROM 
    ciclista c
  
  WHERE 
    c.nomequipo='Banesto' OR c.nomequipo='Kelme';

-- modificar con los operadores extendidos (IN)
  
  SELECT *

  FROM 
    ciclista c
  
  WHERE c.nomequipo IN('Banesto','Kelme');

  -- modificar con los operadores extendidos (NOT)
     
  SELECT *

    FROM 
    ciclista c
  
    WHERE c.nomequipo NOT('Banesto','Kelme');
  