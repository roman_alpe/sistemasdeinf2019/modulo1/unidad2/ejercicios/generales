﻿USE paginaweb;

/** 
  1- Nombre del autor y título del libro
**/
  
/**PRODUCTO CARTESIANO 
**/

SELECT a.nombre_completo, l.titulo 
  FROM libro l,autor a
  WHERE l.autor = a.id_autor;

/**Con JOIN 
**/

SELECT a.nombre_completo, l.titulo 
  FROM libro l JOIN autor a 
  ON l.autor = a.id_autor;

/** JOIN como producto cartesiano (no utilizar)
  **/

SELECT a.nombre_completo, l.titulo 
  FROM libro l JOIN autor a 
  WHERE l.autor = a.id_autor;

/** 
  2- Nombre de los autores y el título del libro en el que han ayudado.
**/
  
  
  SELECT a1.nombre_completo, l.titulo
    FROM libro l 
    JOIN ayuda a ON l.id_libro = a.libro
    JOIN autor a1 ON a.autor = a1.id_autor;

  
  SELECT a1.nombre_completo, l.titulo
    FROM libro l 
    JOIN autor a1 JOIN ayuda a
    ON a1.id_autor = a.autor
    AND l.id_libro = a.libro;

/** 
  3- Los libros que se ha descargado cada usuario con la fecha de descarga.
     Listar el id, fecha de descarga y el login del libro.
**/
    
    SELECT f.libro, f.fecha_descarga,f.usuario
      FROM fechadescarga f; 

/** 
  4- Los libros que se ha descargado cada usuario 
  con la fecha de descarga. 
  Listar el login, correo, fecha descarga y título del libro
**/
     SELECT u.login, u.email, f.fecha_descarga, l.titulo
      FROM fechadescarga f 
      JOIN descarga d ON f.libro = d.libro AND f.usuario = d.usuario
      JOIN usuario u ON d.usuario = u.login
      JOIN libro l ON d.libro = l.id_libro;  

    SELECT * 
      FROM fechadescarga f JOIN descarga d JOIN usuario u JOIN libro l
      ON f.libro=d.libro AND f.usuario=d.usuario
      AND d.usuario=u.login AND d.libro=l.id_libro;


     /**
     5- Numero de libros 
      
     **/

    SELECT COUNT(*) nlibros 
    FROM libro l;

    /**
     6- Numero de libros por coleccion
      
     **/
    SELECT l.coleccion, COUNT(*) nlibros 
      FROM libro l
      GROUP BY l.coleccion;

     /**
     7- La colección que tiene más libros
      
     **/
    
    -- Subconsulta C1
      
                 
      SELECT l.coleccion, COUNT(*) nlibros 
      FROM libro l
      GROUP BY l.coleccion);
      
    -- Subconsulta C2
    
    SELECT MAX(c1.nlibros) maximo FROM

      (
        SELECT l.coleccion, COUNT(*) nlibros 
        FROM libro l
        GROUP BY l.coleccion
      ) c1;
    
    -- Consulta Final
      
     SELECT c1.coleccion 
       FROM (
        SELECT l.coleccion, COUNT(*) nlibros 
        FROM libro l
        GROUP BY l.coleccion
      ) c1 
      JOIN (
            SELECT MAX(c1.nlibros) maximo FROM

              (
                SELECT l.coleccion, COUNT(*) nlibros 
                FROM libro l
                GROUP BY l.coleccion
              ) c1  
            ) c2
      ON c1.nlibros=c2.maximo;

    -- Consulta Final con vistas
         SELECT * FROM  c1 JOIN c2 ON c2.maximo;

    
    -- Final con un where


      SELECT * FROM 
        (
          SELECT l.coleccion, COUNT(*) nlibros 
          FROM libro l
          GROUP BY l.coleccion
        ) c1 
         WHERE nlibros=
        (
          SELECT MAX(c1.nlibros) maximo FROM
          (
            SELECT l.coleccion,COUNT(*) nlibros 
                FROM libro l
                GROUP BY l.coleccion
           ) c1 
        );

          -- Final con un having

        SELECT l.coleccion, COUNT(*) nlibros 
          FROM libro l
          GROUP BY l.coleccion
          HAVING nlibros=
        (
          SELECT MAX(c1.nlibros) maximo FROM
          (
            SELECT l.coleccion,COUNT(*) nlibros 
                FROM libro l
                GROUP BY l.coleccion
           ) c1 
         );
        
           
    
    
    /** 
      8 - Colección con menos libros 
     
     **/
    
      SELECT c1.coleccion 
       FROM (
        SELECT l.coleccion, COUNT(*) nlibros 
        FROM libro l
        GROUP BY l.coleccion
      ) c1; 
      

      -- Consulta final

        SELECT c1.coleccion 
       FROM (
        SELECT l.coleccion, COUNT(*) nlibros 
        FROM libro l
        GROUP BY l.coleccion
      ) c1 
      JOIN (
            SELECT MIN(c1.nlibros) minimo FROM

              (
                SELECT l.coleccion, COUNT(*) nlibros 
                FROM libro l
                GROUP BY l.coleccion
              ) c1  
            ) c2
      ON c1.nlibros=c2.minimo;


/**
  9- El nombre de la colección que tiene más libros
**/


SELECT c.nombre FROM coleccion c
  JOIN (
          SELECT c1.coleccion  
       FROM (
        SELECT l.coleccion, COUNT(*) nlibros 
        FROM libro l
        GROUP BY l.coleccion
      ) c1 
      JOIN (
            SELECT MAX(c1.nlibros) maximo FROM

              (
                SELECT l.coleccion, COUNT(*) nlibros 
                FROM libro l
                GROUP BY l.coleccion
              ) c1  
      ) c2
      ON c1.nlibros=c2.maximo
  
  ) consulta7
  ON c.id_coleccion= consulta7.coleccion;


/**
  10- El nombre de la colección que tiene menos libros
**/      

SELECT c.nombre FROM coleccion c
  JOIN (
          SELECT c1.coleccion  
       FROM (
        SELECT l.coleccion, COUNT(*) nlibros 
        FROM libro l
        GROUP BY l.coleccion
      ) c1 
      JOIN (
            SELECT MIN(c1.nlibros) minimo FROM

              (
                SELECT l.coleccion, COUNT(*) nlibros 
                FROM libro l
                GROUP BY l.coleccion
              ) c1  
      ) c2
      ON c1.nlibros=c2.minimo
  
  ) consulta8
  ON c.id_coleccion= consulta8.coleccion;

/**
  11- El nombre del libro que se ha descargado más veces
**/ 
   

  -- c1
  -- el numero de descargas por cada libro
  
  
  SELECT f.libro, COUNT(*) ndescargas 
    FROM  fechadescarga f
    GROUP BY f.libro;
  
  -- c2
  -- el numero máximo de descargas por cada libro
    
    SELECT MAX(c1.ndescargas) maximo FROM (
      SELECT f.libro,COUNT(*) ndescargas 
      FROM  fechadescarga f
      GROUP BY f.libro
    ) c1;
 
  -- c3 
  -- el id del libro que se ha descargado más veces
  
  SELECT c1.libro FROM (
    SELECT f.libro, COUNT(*) ndescargas 
    FROM  fechadescarga f
    GROUP BY f.libro
    ) c1
    JOIN(
        SELECT MAX(c1.ndescargas) maximo FROM (
              SELECT f.libro,COUNT(*) ndescargas 
              FROM  fechadescarga f
              GROUP BY f.libro
             ) c1
    ) c2
    ON c1.ndescargas=c2.maximo; 

  -- Final
    
    SELECT l.titulo FROM libro l
      JOIN (
              SELECT c1.libro FROM (
                  SELECT f.libro, COUNT(*) ndescargas 
                  FROM  fechadescarga f
                  GROUP BY f.libro
                  ) c1
                  JOIN(
                      SELECT MAX(c1.ndescargas) maximo FROM (
                            SELECT f.libro,COUNT(*) ndescargas 
                            FROM  fechadescarga f
                            GROUP BY f.libro
                           ) c1
                  ) c2
                  ON c1.ndescargas=c2.maximo
          ) c3
      ON c3.libro= l.id_libro;

-- 12 El nombre del usuario que ha descargado más libros

  -- c1 
  -- el numero de descargas por cada usuario
  
  
  SELECT f.usuario, COUNT(*) ndescargas 
    FROM  fechadescarga f
    GROUP BY f.usuario;

  --c2
  -- el número máximo de descargas por cada usuario
  SELECT MAX(c1.ndescargas) maximo FROM (
      SELECT f.usuario,COUNT(*) ndescargas 
      FROM  fechadescarga f
      GROUP BY f.usuario
    ) c1;    
  
  -- c3 
  -- el login del usuario que ha descargado más veces    
  SELECT c1.usuario FROM (
    SELECT f.usuario, COUNT(*) ndescargas 
    FROM  fechadescarga f
    GROUP BY f.usuario
    ) c1
    JOIN(
        SELECT MAX(c1.ndescargas) maximo FROM (
              SELECT f.usuario,COUNT(*) ndescargas 
              FROM  fechadescarga f
              GROUP BY f.usuario
             ) c1
    ) c2
    ON c1.ndescargas=c2.maximo;

  -- Final

    SELECT u.login FROM usuario u
      JOIN (
      SELECT c1.usuario FROM (
    SELECT f.usuario, COUNT(*) ndescargas 
    FROM  fechadescarga f
    GROUP BY f.usuario
    ) c1
    JOIN(
        SELECT MAX(c1.ndescargas) maximo FROM (
              SELECT f.usuario,COUNT(*) ndescargas 
              FROM  fechadescarga f
              GROUP BY f.usuario
             ) c1
    ) c2
    ON c1.ndescargas=c2.maximo
    
      
    ) c3
    ON c3.usuario= u.login;


-- Final con Having
  
  SELECT f.usuario, COUNT(*) ndescargas 
    FROM  fechadescarga f
    GROUP BY f.usuario
    HAVING ndescargas=
    (
      SELECT MAX(c1.ndescargas) maximo FROM 
        (
      
          SELECT f.usuario,COUNT(*) ndescargas 
          FROM  fechadescarga f
          GROUP BY f.usuario
        ) c1
    );





    /** 13- El nommbre de los usuarios que han descargado más libros que Adam3**/

      -- c1 Nº de descargas de Adam3
      SELECT COUNT(*) ndescargas 
        FROM fechadescarga f
        WHERE f.usuario='Adam3';
      
      
      -- c2 Descargas de cada usuario
      SELECT f.usuario, COUNT(*) nlibros 
        FROM fechadescarga f 
        GROUP BY f.usuario;
     
     -- Consulta Final
      SELECT f.usuario, COUNT(*) nlibros 
        FROM fechadescarga f 
        GROUP BY f.usuario
        HAVING nlibros>(
          SELECT COUNT(*) ndescargas 
          FROM fechadescarga f
          WHERE f.usuario='Adam3'
        );

     
     
     -- c1
      SELECT f.usuario, COUNT(*) nlibros 
        FROM fechadescarga f 
        GROUP BY f.usuario;
     
     
     
     -- c2
      SELECT c1.ndescargas 
        FROM (
          SELECT f.usuario, COUNT(*)ndescargas 
            FROM fechadescarga f 
            GROUP BY f.usuario
          ) c1 
        WHERE c1.usuario = 'Adam3';

  -- Consulta final
    SELECT c1.usuario 
    FROM (
      SELECT f.usuario, COUNT(*)ndescargas 
        FROM fechadescarga f 
        GROUP BY f.usuario
      ) c1 
    JOIN(
      SELECT c1.ndescargas 
        FROM (
          SELECT f.usuario, COUNT(*)ndescargas 
            FROM fechadescarga f 
            GROUP BY f.usuario
          ) c1 
        WHERE c1.usuario = 'Adam3'
      )c2 
    ON c1.ndescargas > c2.ndescargas;

    
-- Consulta final (Where)
  SELECT c1.usuario 
    FROM (
      SELECT f.usuario, COUNT(*)ndescargas 
        FROM fechadescarga f 
        GROUP BY f.usuario
      ) c1 
    WHERE c1.ndescargas > (
      SELECT c1.numDescargas 
        FROM (
          SELECT f.usuario, COUNT(*)ndescargas 
            FROM fechadescarga f 
            GROUP BY f.usuario
          ) c1 
        WHERE c1.usuario = 'Adam3');


  /** 14- El mes que más libros se han descargado**/
-- c1 
  -- el número de descargas por cada mes 
  
--  C1 
  SELECT MONTH(f.fecha_descarga)mes, COUNT(*)ndescargasmes 
    FROM fechadescarga f 
    GROUP BY MONTH(f.fecha_descarga);

  --  C2 Nº máximo de descargas por mes
  SELECT MAX(c1.descargasmes)maximo 
    FROM (
      SELECT MONTH(f.fecha_descarga)mes, COUNT(*)ndescargasmes 
        FROM fechadescarga f 
        GROUP BY MONTH(f.fecha_descarga)
    ) c1;
      
-- Final con JOIN
  SELECT c1.mes 
    FROM (
      SELECT MONTH(f.fecha_descarga)mes, COUNT(*)ndescargasmes 
        FROM fechadescarga f 
        GROUP BY MONTH(f.fecha_descarga)
    )c1 
    JOIN (
      SELECT MAX(c1.ndescargasmes)maximo 
        FROM (
          SELECT MONTH(f.fecha_descarga)mes, COUNT(*)ndescargasmes 
            FROM fechadescarga f 
            GROUP BY MONTH(f.fecha_descarga)
        ) c1
    ) c2 
    ON c1.ndescargasmes = c2.maximo;

  -- Final con HAVING
 SELECT MONTH(f.fecha_descarga)mes, COUNT(*)ndescargasmes 
    FROM fechadescarga f 
    GROUP BY MONTH(f.fecha_descarga)
    HAVING ndescargasmes=
    (
      SELECT MAX(c1.ndescargasmes)maximo 
        FROM (
          SELECT MONTH(f.fecha_descarga)mes, COUNT(*)ndescargasmes 
            FROM fechadescarga f 
            GROUP BY MONTH(f.fecha_descarga)
      ) c1
    );







);












 

