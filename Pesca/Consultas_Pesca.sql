﻿USE pesca;

/*
  1- Indica el nombre del club que tiene pescadores */
  
 SELECT DISTINCT c.nombre 
  FROM clubes c JOIN pescadores p 
  ON c.cif = p.club_cif;

/*
  2- Indica el nombre del club que no tiene pescadores */

  SELECT c.nombre
    FROM clubes c 
    LEFT JOIN pescadores p ON c.cif = p.club_cif
    WHERE p.club_cif IS NULL;

/*
  3- Indica el nombre de cotos autorizados a algún club */
  
  SELECT DISTINCT a.coto_nombre
    FROM autorizados a;
  
 /* Con JOIN (Consulta ralentizada) */
  
  SELECT c.nombre
    FROM cotos c 
    JOIN autorizados a ON c.nombre = a.coto_nombre; 
  
  /*4 - Indica la provincia que tiene cotos autorizados */

  SELECT DISTINCT c.provincia
    FROM cotos c 
    JOIN autorizados a ON c.nombre = a.coto_nombre;
  
  /*5- Indica el nombre de cotos que no están autorizados a ningún club */
    
    SELECT c.nombre 
      FROM cotos c
      LEFT JOIN autorizados a ON c.nombre = a.coto_nombre
      WHERE a.coto_nombre IS NULL;
  
  /*6- Indica el nº de ríos por provincia con cotos*/
    
    SELECT c.provincia, COUNT(DISTINCT c.rio)
      FROM cotos c
      GROUP BY c.provincia;
  
  /*7- Indica el nº de ríos por provincia con cotos autorizados*/
      
    SELECT c.provincia, COUNT(DISTINCT c.rio) nº_rios 
      FROM cotos c JOIN autorizados a 
      ON c.nombre = a.coto_nombre
      GROUP BY c.provincia;    
   
   /*8- Indica el nombre de la provincia con más cotos autorizados*/
 
   -- C1. Nº cotos autorizados por provincia
    
    SELECT c.provincia, COUNT(DISTINCT a.coto_nombre) 
      FROM cotos c
      JOIN autorizados a ON c.nombre = a.coto_nombre
      GROUP BY c.provincia;
    
    
   -- C2. Nº máximo de cotos autorizados por provincia
   
   SELECT MAX(c1.provincia) maximo 
    FROM 
    (
      SELECT c.provincia, COUNT(DISTINCT a.coto_nombre) 
      FROM cotos c
      JOIN autorizados a ON c.nombre = a.coto_nombre
      GROUP BY c.provincia
     )c1;
  
  
  -- C. Final
    
    SELECT c1.provincia FROM 
      (
        SELECT c.provincia, COUNT(DISTINCT a.coto_nombre) 
              FROM cotos c
              JOIN autorizados a ON c.nombre = a.coto_nombre
              GROUP BY c.provincia
              )c1
      JOIN
      (
        SELECT MAX(c1.provincia) maximo 
            FROM 
            (
              SELECT c.provincia, COUNT(DISTINCT a.coto_nombre) 
              FROM cotos c
              JOIN autorizados a ON c.nombre = a.coto_nombre
              GROUP BY c.provincia
             )c1

      )c2
      ON c1.provincia= c2.maximo;

    -- 9. Indica el nombre del padrino y su ahijado.
    
    SELECT padrinos.numSocio, padrinos.nombre, padrinos.dni, a.*,
            ahijados.numSocio,ahijados.dni,ahijados.nombre 
      FROM pescadores padrinos 
      JOIN apadrinar a ON padrinos.numSocio = a.padrino
      JOIN pescadores ahijados ON ahijados.numsocio=a.ahijado;

    SELECT padrinos.nombre,ahijados.nombre 
      FROM pescadores padrinos 
      JOIN apadrinar a ON padrinos.numSocio = a.padrino
      JOIN pescadores ahijados ON ahijados.numsocio=a.ahijado;

    -- 10. Indica el nº de ahijados de cada pescador.

    SELECT a.padrino, COUNT(*) nº_ahijados 
      FROM apadrinar a
      GROUP BY a.padrino;