﻿USE ciclistas;

/** ciclistas que han ganado etapas
  Mostrar dorsal**/
SELECT DISTINCT e.dorsal FROM etapa e;





/** ciclistas que han ganado puertos
  mostrar dorsal **/
    
SELECT DISTINCT p.dorsal FROM puerto p;

/** ciclistas que han ganado etapas o puertos
  mostrar dorsal **/

  SELECT DISTINCT e.dorsal FROM etapa e
    UNION 
  SELECT DISTINCT p.dorsal FROM puerto p;

  SELECT DISTINCT e.dorsal FROM etapa e
    UNION ALL
  SELECT DISTINCT p.dorsal FROM puerto p;
  
  /** ciclistas que han ganado etapas y puertos
  mostrar dorsal **/
  
  SELECT DISTINCT e.dorsal FROM etapa e
    UNION 
  SELECT DISTINCT p.dorsal FROM puerto p;

  /** Listado de todas las etapas y
    el ciclista que las ha ganado **/

  SELECT * 
    FROM ciclista c JOIN etapa e 
    ON c.dorsal = e.dorsal;

/** Listado de todos los puertos 
  y el ciclista que los ha ganado. **/

SELECT * 
  FROM puerto p JOIN ciclista c 
  ON p.dorsal = c.dorsal;

/** Listado de todos los puertos 
  y el ciclista >30 que los ha ganado. 

SELECT * 
  FROM puerto p JOIN ciclista c 
  ON p.dorsal = c.dorsal
  WHERE p.dorsal=c.dorsal
edad>30; **/

  SELECT c1.dorsalEtapa dorsal FROM 
    (SELECT DISTINCT e.dorsal dorsalEtapa FROM etapa e) c1
    JOIN
    (SELECT DISTINCT p.dorsal dorsalPuerto FROM puerto p) c2
    ON c1.dorsalEtapa=c2.dorsalPuerto;

  /** Combinaciones Internas
    
  **/
  --  Listar todos los ciclistas con todos 
  -- los datos del equipo al que pertenecen.

SELECT * 
  FROM equipo e JOIN ciclista c 
  ON e.nomequipo = c.nomequipo;

-- Con un campo menos
SELECT * 
  FROM  equipo e JOIN ciclista c
  USING(nomequipo);

-- No recomendable
SELECT * 
  FROM equipo e JOIN ciclista c 
  WHERE e.nomequipo = c.nomequipo;

/*
  Producto Cartesiano
SELECT * 
    FROM ciclista c, equipo e;

*/
     
-- Convertir el producto cartesiano en una combinación interna  
  
  SELECT * 
    FROM ciclista c, equipo e
    WHERE e.nomequipo = c.nomequipo;

  /*
  listarme los nombres de los ciclistas y del equipo de aquellos ciclistas que hayan ganado puertos  
  */
SELECT DISTINCT c.nombre,c.nomequipo 
  FROM puerto p JOIN ciclista c ON p.dorsal = c.dorsal;

/*
  listarme los nombres de los ciclistas y del equipo de aquellos ciclistas que hayan ganado etapas  
 */
  SELECT DISTINCT c.nombre,c.nomequipo 
  FROM etapa e JOIN ciclista c 
  ON e.dorsal = c.dorsal;
  
  SELECT * 
    FROM etapa e JOIN ciclista c
    USING(dorsal);
 
 -- c1 (Subconsulta)
    
    SELECT DISTINCT e.dorsal
    FROM etapa e;
 
 -- Consulta Completa
  
  SELECT c.nombre,c.nomequipo
    FROM ciclista c 
    JOIN(SELECT DISTINCT e.dorsal FROM etapa e) c1
    USING(dorsal);
  
  /** 
  Quiero saber los ciclistas que han ganado puertos y el numero de puertos que ha ganado. Del ciclista quiere saber el dorsal y el nombre
    (dorsal, nombre,numero puertos)  
  **/
--c1
    
    SELECT DISTINCT c.dorsal,p.nompuerto 
    FROM puerto p JOIN ciclista c USING(dorsal)

-- Consulta completa
  
  SELECT c1.dorsal,c1.nombre,COUNT(*) numeroPuertos
    FROM (
      SELECT c.dorsal,c.nombre,p.nompuerto
      FROM puerto p JOIN ciclista c USING(dorsal)
    ) c1
    GROUP BY c1.dorsal,c1.nombre;
